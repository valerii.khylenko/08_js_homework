const elmParag = document.querySelectorAll('p');
elmParag.forEach((p) => p.style.backgroundColor = '#ff0000')
console.log(elmParag);

const elmId = document.querySelector('#optionsList');
console.log(elmId);
const parentElmId = elmId.parentElement;
console.log(parentElmId);
for (let node of elmId.children) {
    console.log(node.nodeName, node.nodeType);
};

const elmСhangeText = document.getElementById('testParagraph');
elmСhangeText.innerHTML = 'This is a paragraph';
console.log(elmСhangeText);

const elmMainHeader = document.querySelector('.main-header').children;
for (let i = 0; i < elmMainHeader.length; i++) {
    console.log(elmMainHeader[i]);
    elmMainHeader[i].classList.add('nav-item');
};

const elmSectTitle = document.getElementsByClassName('section-title');
for (let i = 0; i < elmSectTitle.length; i++) {
    elmSectTitle[i].remove('section-title');
};
console.log(elmSectTitle);